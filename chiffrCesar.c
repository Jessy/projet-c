/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : Sujet n°3                                                    *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : MONROCQ-Jessy                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : chiffrCesar.c                                             *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "chiffrCesar.h"
#define MAX_LETTRE 100

void chiffrer(char msg[MAX_LETTRE]) {
	
	int taille = strlen(msg);
	int i = 0;
	while(i<taille) {
		if(msg[i]!=32) {
			if(msg[i]<88 && msg[i]>64 || msg[i]<120 && msg[i]>96) {
				msg[i]=msg[i]+3;
			} else {
				msg[i]=msg[i]-23;
			}
		}
		i++;
	}
}

void dechiffrer(char msg[MAX_LETTRE]) {
	
	int taille = strlen(msg);
	int i = 0;
	while(i<taille) {
		if(msg[i]!=32) {
			if(msg[i]<91 && msg[i]>67 || msg[i]<123 && msg[i]>99) {
				msg[i]=msg[i]-3;
			} else {
				msg[i]=msg[i]+23;
			}
		}
		i++;
	}
}