/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : Sujet n°3                                                    *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : MONROCQ-Jessy                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : verifAlpha.c                                              *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "verifAlpha.h"

int verifier(char msg[100]) {

	int taille = strlen(msg);
	int i = 0;
	int v = 1;
	while(v==1 && i<taille) {
		int asciiVal = msg[i];
		if(asciiVal<65 || asciiVal>122) {
			v=0;
		}
		if(asciiVal>90 && asciiVal<97) {
			v=0;
		}
		if(asciiVal==32) {
			v=1;
		}
		i++;
	}
	return v;
}