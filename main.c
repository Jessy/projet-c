/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : Sujet n°3                                                    *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : MONROCQ-Jessy                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : main.c                                                    *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "verifAlpha.h"
#include "chiffrCesar.h"
#define MAX_LETTRE 100

void main() {
	char msg[MAX_LETTRE];
	printf("Saisir un texte au clavier (max : 100 lettres)\n");
	scanf("%[^\n]", msg);
	if(strlen(msg)>=MAX_LETTRE) {
		printf("Erreur! Le mot est trop long!\n");
	}
	else {
	if(verifier(msg) == 1) {
		printf("Le texte est correct, pas de caractères spéciaux\n\n");
		chiffrer(msg);
		printf("Le texte chiffré est : %s\n", msg);
		dechiffrer(msg);
		printf("Le texte d'origine est : %s\n", msg);
	}
	else {
		printf("Erreur! Caractères spéciaux détectés!\n");
	}
	}

}