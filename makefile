
G = gcc

all: main

main : main.c verifAlpha.o chiffrCesar.o
	$(G) $^ -o $@

%.o: %.c %.h
	$(G) -c $<