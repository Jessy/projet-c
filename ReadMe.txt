/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : Sujet n°3                                                    *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : MONROCQ-Jessy                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : ReadMe	                                              *
*                                                                             *
******************************************************************************/

Ce projet a pour but de chiffrer un texte par l'algorithme de Cesar qui consiste
à décaler une lettre de l'alphabet de 3 positions. (Ex: a => d  et  z => c).

/main.c
--> ce fichier contient le code principale du projet ainsi que l'affichage et les demandes à l'utilisateur:
J'ai defini une variable MAX_LETTRE de 100 pour la taille maximale du texte a chiffrer.
On crée une chaine de caracteres msg[] dans laquelle on va introduire le texte de l'utilisateur par
la fonction scanf(). Ensuite on verifie que le texte ne depasse pas les 100 caracteres sinon on arrete le
programme. Ensuite on utilise une fonction de verification du texte verifier() qui se trouve dans le fichier
verifAlpha.c. Si le texte ne comporte pas de caracteres speciaux on peut continuer l'operation, sinon
on arrete le programme. Enfin, on utilise la fonction chiffrer() et dechiffrer() du fichier chiffCesar.c
pour chiffrer et dechiffrer le texte qu'on affiche ensuite avec un printf().

/verifAlpha.c
--> ce fichier contient le code c de verification  alphanumerique du texte a chiffrer. On a donc une
fonction int verifier(char msg[100]) qui prend une chaine de caracteres en entree et renvoie un entier.
Pour valider la verification, j'ai decide d'utiliser les entier comme booleen (1 = vrai , 0 = faux).
La boucle while verifie que la valeur ASCII de chaque caracteres du texte est bien celle des lettres
majuscules ou minuscules ou encore celle de la touche espace.

/chiffrCesar.c
--> ce fichier contient le code c de chiffrement et dechiffrement du texte une fois verifie.
J'ai a nouveau defini une variable MAX_LETTRE de 100 pour la taille du texte.
On a cette fois-ci deux fonctions : void chiffrer(char msg[MAX_LETTRE] et void dechiffrer(char msg[MAX_LETTRE]).
Plutot que de renvoyer un nouveau texte chiffrer, on modifie le texte original que l'on peux ensuite retranscrire
dans la fonction dechiffrer. Pour cela, on ajoute 3 a la valeur ASCII des lettres, majuscules ou minuscules, 
comprises avant la valeur ASCII de la lettre x. Sinon, on enleve 23 a la valeur ASCII de ces lettres.
Ansi on obtient un parfait decalage en conservant les majuscules/minuscules.
Pour la fonction dechiffrer il s'agit simplement de refaire le meme travail a l'envers.

/makefile
--> ce fichier contient les codes de compilation du projet avec la commande make main/make all.